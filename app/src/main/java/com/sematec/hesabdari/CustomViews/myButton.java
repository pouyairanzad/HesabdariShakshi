package com.sematec.hesabdari.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class myButton extends AppCompatButton {
    public myButton(Context context) {
        super(context);
        setTypeFace(context);
    }

    public myButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context);
    }

    public myButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context);
    }

    void setTypeFace(Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/BZAR.TTF");
        this.setTypeface(tf);
    }
}