package com.sematec.hesabdari.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class myTextView extends AppCompatTextView {
    public myTextView(Context context) {
        super(context);
        setTypeFace(context);
    }

    public myTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context);
    }

    public myTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context);
    }

    void setTypeFace(Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/ANegaarBold.ttf");
        this.setTypeface(tf);

    }
}
