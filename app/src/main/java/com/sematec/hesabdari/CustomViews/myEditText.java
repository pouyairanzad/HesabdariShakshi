package com.sematec.hesabdari.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class myEditText extends AppCompatEditText {
    public myEditText(Context context) {
        super(context);
        setTypeFace(context);
    }

    public myEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context);
    }

    public myEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context);
    }
    void setTypeFace(Context mContext) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/BNARM.TTF");
        this.setTypeface(tf);
    }
    public String text(){
        return this.getText().toString() ;
    }

}
