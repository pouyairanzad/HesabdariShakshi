package com.sematec.hesabdari;

import com.orhanobut.hawk.Hawk;
import com.orm.SugarApp;

public class MyApp extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        //Hawk.init(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}

