package com.sematec.hesabdari.DaramadPage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sematec.hesabdari.BaseActivity;
import com.sematec.hesabdari.CustomViews.myEditText;
import com.sematec.hesabdari.DastebandiPage.DastebandiAsli;
import com.sematec.hesabdari.HesabPage.HesabActivity;
import com.sematec.hesabdari.R;


import java.text.NumberFormat;
import java.util.Locale;

public class DaramadHazineActivity extends BaseActivity {
   myEditText editTextMablagh;
    myEditText  editTextDastebandi;
    myEditText  editTextHesab;
    myEditText   editTextTarikh;
    myEditText editTextTozih;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daramad_hazine);
        bind();
    }
    void bind (){
         editTextMablagh=findViewById(R.id.editTextMablagh);
        editTextDastebandi=findViewById(R.id.editTextDastebandi);
         editTextHesab=findViewById(R.id.editTextHesab);
           editTextTarikh=findViewById(R.id.editTextTarikh);
        editTextTozih=findViewById(R.id. editTextTozih);
        editTextMablagh.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (editTextMablagh.getText().length()==0){return;}
                if(b==true)
                {
                    Double mablagh =Double.parseDouble(editTextMablagh.getText().toString());
                    editTextMablagh.setText(NumberFormat.getNumberInstance(Locale.US).format(mablagh));

                }
                else
                {
                    String mablaghbedonejodadkonande = editTextMablagh.getText().toString().replace(",","");
                    editTextMablagh.setText(mablaghbedonejodadkonande);
                }
            }
        });
        editTextHesab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext , HesabActivity.class);
                startActivityForResult(intent,0);

            }
        });
        editTextDastebandi.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( mContext, DastebandiAsli.class );
                startActivityForResult( intent,0 );
            }
        } );


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       if(resultCode==2){

           int id = data.getIntExtra("id_hesab",getTaskId() );
           String name_hesab= data.getStringExtra( "namehesab" );
           editTextHesab.setText( name_hesab );



       }
    }


}
