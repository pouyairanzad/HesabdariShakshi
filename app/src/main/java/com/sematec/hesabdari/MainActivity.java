package com.sematec.hesabdari;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sematec.hesabdari.CustomViews.myTextView;
import com.sematec.hesabdari.DaramadPage.DaramadHazineActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext=this;
    myTextView textViewMabnayeTarikhiNemodar;
    myTextView textView1;
    myTextView textViewNoeNemodar;
    myTextView textViewAdadiMablagh;
    myTextView textViewMablagh;
    myTextView textViewHeaderTitle;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
       findViewById(R.id.buttonHazine).setOnClickListener(this);
        findViewById(R.id.buttonDaramad).setOnClickListener(this);

    }

    void bind() {
        textViewMabnayeTarikhiNemodar = findViewById(R.id.textViewMabnayeTarikhiNemodar);
        textView1 = findViewById(R.id.textView1);
        textViewNoeNemodar = findViewById(R.id.textViewNoeNemodar);
        textViewAdadiMablagh = findViewById(R.id.textViewAdadiMablagh);
        textViewAdadiMablagh = findViewById(R.id.textViewAdadiMablagh);
        textViewMablagh = findViewById(R.id.textViewMablagh);
        textViewHeaderTitle = findViewById(R.id.textViewHeaderTitle);


    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.buttonDaramad){
            Intent intent = new Intent(mContext, DaramadHazineActivity.class);
            startActivity(intent);
        }
        if(view.getId()==R.id.buttonHazine){}

    }
}
