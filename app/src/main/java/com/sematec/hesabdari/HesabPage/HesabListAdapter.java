package com.sematec.hesabdari.HesabPage;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sematec.hesabdari.CustomViews.myTextView;
import com.sematec.hesabdari.R;

import java.util.List;


public class HesabListAdapter extends BaseAdapter {
    Context mContext;
    List<HesabModels> list_hesabha;

    public HesabListAdapter(Context mContext,   List<HesabModels> list_hesabha) {
        this.mContext = mContext;
        this.list_hesabha = list_hesabha;
    }

    @Override
    public int getCount() {
        return list_hesabha.size();
    }

    @Override
    public Object getItem(int position) {
        return list_hesabha.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext)
                .inflate( R.layout.hesab_row, viewGroup, false);

        myTextView  namehesab = row.findViewById(R.id.namehesabrow);
        //myTextView mojodi = row.findViewById(R.id.Mojodirow);


        namehesab.setText(list_hesabha.get(position).getNameHesab());
        //mojodi.setText(list_hesabha.get(position).getMojodi()+ "");
        return row;
    }
}














