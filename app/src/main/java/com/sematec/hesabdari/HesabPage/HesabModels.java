package com.sematec.hesabdari.HesabPage;



public class HesabModels
{
    public HesabModels(int id, String nameHesab, Double mojodi) {
        this.id = id;
        NameHesab = nameHesab;
        Mojodi = mojodi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int id ;
    public String NameHesab ;
    public  Double Mojodi ;


    public HesabModels() {

    }


    public String getNameHesab() {
        return NameHesab;
    }

    public void setNameHesab(String nameHesab) {
        this.NameHesab = nameHesab;
    }

    public Double getMojodi() {
        return Mojodi;
    }

    public void setMojodi(Double mojodi) {
        this.Mojodi = mojodi;
    }
}

