package com.sematec.hesabdari.HesabPage;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sematec.hesabdari.CustomViews.myButton;
import com.sematec.hesabdari.CustomViews.myEditText;
import com.sematec.hesabdari.CustomViews.myTextView;
import com.sematec.hesabdari.PublicMethods;
import com.sematec.hesabdari.R;


import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class HesabActivity extends AppCompatActivity {

    Context mContext = this;
    myEditText NameHesab;
    myEditText Moojodi;
    ListView listView;
    List<HesabModels> List_hesbaba;
    myButton ButtonSave;
    DataBasemanger db;
    //RecyclerView recyclerView ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_hesab );


        bind();
        db = new DataBasemanger( this, "hesabdariDB", null, 1 );
        loadHesab();
        registerForContextMenu( listView );


    }


    void bind() {
        //recyclerView=findViewById(R.id.recyleid);
        NameHesab = findViewById( R.id.EditTextNameHesab );
        listView = findViewById( R.id.recyleid );
        Moojodi = findViewById( R.id.EditTextMojodi );
        ButtonSave = findViewById( R.id.btnSave );


        ButtonSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        } );

        Moojodi.setOnFocusChangeListener( new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                virgol( b );

            }
        } );
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                int id_hesab = List_hesbaba.get( position ).id;
                String Namehesab = List_hesbaba.get( position ).NameHesab;
                Intent intent = new Intent();
                intent.putExtra( "id_hesab",id_hesab );
                intent.putExtra( "namehesab", Namehesab );
                setResult( 2,intent );
                finish();
            }
        } );


    }


    private void save() {
        String nameV = NameHesab.text();
        Double MojodiV = Double.parseDouble( Moojodi.text() );
        db.insert( nameV, MojodiV );


        cleanForm();
        PublicMethods.toast( mContext, "حساب جدید اضافه شد " );
        loadHesab();


    }


    void cleanForm() {
        NameHesab.setText( "" );
        Moojodi.setText( "" );

    }

    public void loadHesab() {
        List_hesbaba = db.getHesab();
        HesabListAdapter adapter = new HesabListAdapter(
                mContext, List_hesbaba );
        listView.setAdapter( adapter );

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu( menu, v, menuInfo );
        menu.add( "حذف" );
        menu.add( "ویرایش" );

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle( List_hesbaba.get( info.position ).NameHesab );


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getTitle() == "حذف") {
            final Dialog dialog = new Dialog( HesabActivity.this );
            dialog.setContentView( R.layout.soal_dialog );
            dialog.setTitle( "حذف حساب " );
            myTextView TextViewSoal = dialog.findViewById( R.id.textViewSoal );
            final int id = List_hesbaba.get( info.position ).id;
            TextViewSoal.setText( "با حذف این حساب تمام تراکنش های شما حذف می شود،ایا با انجام این عملیات  موافق هستید" );
            dialog.show();

            myButton ButtonBale = dialog.findViewById( R.id.buttonBale );
            myButton ButtonEnseraf = dialog.findViewById( R.id.buttonEnseraf );
            ButtonBale.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    db.deleteHesab( id );
                    loadHesab();
                    dialog.dismiss();
                    PublicMethods.toast( mContext, "حساب شما حذف شد " );

                }
            } );
            ButtonEnseraf.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            } );


        }
        if (item.getTitle() == "ویرایش") {
            final Dialog dialog = new Dialog( HesabActivity.this );
            dialog.setContentView( R.layout.virayesh_dilog );
            dialog.setTitle( "ویرایش حساب " );
            final myEditText EditTextVirayesh = dialog.findViewById( R.id.editTextNameHesab );
            final int id_hesab = List_hesbaba.get( info.position ).id;
            String NameHesab = List_hesbaba.get( info.position ).NameHesab;
            EditTextVirayesh.setText( NameHesab );
            dialog.show();
            myButton ButtonBerozresani = dialog.findViewById( R.id.buttonBerozresani );
            myButton ButtonEnseraf = dialog.findViewById( R.id.buttonEnseraf );
            ButtonBerozresani.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (EditTextVirayesh.getText().length() == 0) {
                        PublicMethods.toast( mContext, " نام حساب وارد کنید " );
                    } else {
                        db.updateHesab( EditTextVirayesh.text(), id_hesab );
                        dialog.dismiss();
                        loadHesab();
                        PublicMethods.toast( mContext, "حساب شما ویرایش شد " );

                    }


                }
            } );
            ButtonEnseraf.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            } );


        }

        return true;
    }


    void virgol(boolean b) {
        if (Moojodi.getText().length() == 0) {
            return;
        }
        if (b == true) {
            Double Mojodi = Double.parseDouble( Moojodi.text() );
            Moojodi.setText( NumberFormat.getNumberInstance( Locale.US ).format( Mojodi ) );


        } else {
            String mablaghbedonejodadkonande = Moojodi.text().replace( ",", "" );
            Moojodi.setText( mablaghbedonejodadkonande );
        }
    }


}










