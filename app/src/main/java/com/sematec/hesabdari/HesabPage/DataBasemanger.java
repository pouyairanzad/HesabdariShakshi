package com.sematec.hesabdari.HesabPage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;


public class DataBasemanger extends SQLiteOpenHelper {
    String makeTableQuery = "" +
            " CREATE TABLE tbl_Hesab ( " +
            " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL\n" +
            "                             UNIQUE  ," +
            " NameHesab TEXT, " +
            "  Mojodi   NUMERIC " +
            " )";

    public DataBasemanger(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(makeTableQuery);




    }
    public void insert(String NameHesab,Double Mojodi) {
        String insertQuery =
                " INSERT INTO tbl_Hesab(NameHesab,Mojodi) " +
                        " VALUES( '" +NameHesab + "', "+ Mojodi +")";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
       sqLiteDatabase.execSQL("DROP TABLE IF EXISTS tbl_Hesab ");
       onCreate(sqLiteDatabase);

    }
   public List<HesabModels> getHesab (){
       List<HesabModels> list_hesabha = new ArrayList<>();
       String query = "SELECT  id ,NameHesab,Mojodi FROM  tbl_Hesab " ;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);

       while (cursor.moveToNext()==true){
          HesabModels h = new HesabModels();
            h.id=cursor.getInt(0);
            h.NameHesab=cursor.getString(1);
             h.Mojodi = cursor.getDouble(2);
           list_hesabha.add(h);


        }
        return  list_hesabha;
    }
    public Boolean deleteHesab(int id){
        Boolean result ;
        String Query= "DELETE FROM  tbl_Hesab WHERE id = " + id;
        try{
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL( Query );
        db.close();
        result=true;}
        catch (Exception ex){
            result=false;
        }
        return result;

    }
    public Boolean updateHesab(String NameHesab, Integer id_hesab)
    {
        Boolean result;

        String query = "UPDATE tbl_hesab " +
                " SET nameHesab = '" + NameHesab + "'" +
                " WHERE id = " + id_hesab;

        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);

            db.close();
            result = true;
        }
        catch (Exception ex)
        {
            result = false;
        }

        return result;
    }
}
